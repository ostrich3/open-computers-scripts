--[[
this script is not updating in the while loop
]]
local component = require("component")
local term = require("term")

local gpu = component.gpu
local bank = component.energy_device

Capacity = bank.getCapacity()
Energy = bank.getEnergy()
Stats = { a = 0, b = 0, c = 0, d = 0, e = 0, f = 0, g = 0, h = 0, i = 0, j = 0 }
TickCount = 0

Width, Height = gpu.getResolution()

local function getPercent()
   diff = Energy / Capacity * 100;
   return diff;
end

local function getDiff()
   diff = getPercent();
   diff = diff / 5;
   diff = math.floor(diff+0.5);
  return diff;
end

local function totalDiff()
   t = "";
   t = Stats.j - Stats.a;
   return t;
end

local function moveVals()
   Stats.a = Stats.b;
   Stats.b = Stats.c;
   Stats.c = Stats.d;
   Stats.d = Stats.e;
   Stats.e = Stats.f;
   Stats.f = Stats.g;
   Stats.g = Stats.h;
   Stats.h = Stats.i;
   Stats.i = Stats.j;
   Stats.j = getDiff();
end

local function centerF(row, msg, ...)
  local mLen = string.len(msg)
  term.setCursor((Width - mLen) / 2, row)
  print(msg:format(...))
end

local function getGraphString(num)
   r = "";
   for i = 0,num,1
   do
      r = r .. "*"
   end
   medV = 20 - num;
   for i = 0,medV,1
   do
      r = r .. "-"
   end
   return r;
end

gpu.setForeground(0xffffff)
term.clear()
term.setCursor(1,1)
centerF(5,  "-----------------------------------------")
centerF(6,  "-       power monitor thing        -")
centerF(7,  "-----------------------------------------")
centerF(8, string.format("- %s -",getGraphString(Stats.j)))
centerF(9, string.format("- %s -",getGraphString(Stats.i)))
centerF(10, string.format("- %s -",getGraphString(Stats.h)))
centerF(11, string.format("- %s -",getGraphString(Stats.g)))
centerF(12, string.format("- %s -",getGraphString(Stats.f)))
centerF(13, string.format("- %s -",getGraphString(Stats.e)))
centerF(14, string.format("- %s -",getGraphString(Stats.d)))
centerF(15, string.format("- %s -",getGraphString(Stats.c)))
centerF(16, string.format("- %s -",getGraphString(Stats.b)))
centerF(17, string.format("- %s -",getGraphString(Stats.a)))
centerF(18, "-----------------------------------------")
centerF(19, string.format("-   power diff:%s                         -", totalDiff()))
centerF(20, string.format("-   power left:%s                         -", getPercent()))
centerF(21, "-----------------------------------------")

while true do

      os.sleep(1)
      moveVals()

      centerF(8, string.format("- %s -",getGraphString(getDiff())))
      centerF(9, string.format("- %s -",getGraphString(Stats.i)))
      centerF(10, string.format("- %s -",getGraphString(Stats.h)))
      centerF(11, string.format("- %s -",getGraphString(Stats.g)))
      centerF(12, string.format("- %s -",getGraphString(Stats.f)))
      centerF(13, string.format("- %s -",getGraphString(Stats.e)))
      centerF(14, string.format("- %s -",getGraphString(Stats.d)))
      centerF(15, string.format("- %s -",getGraphString(Stats.c)))
      centerF(16, string.format("- %s -",getGraphString(Stats.b)))
      centerF(17, string.format("- %s -",getGraphString(Stats.a)))
      centerF(19, string.format("-   power diff:%s                -",totalDiff()))
      centerF(20, string.format("-   power left:%s                -", getPercent()))

end
