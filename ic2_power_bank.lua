--[[
    script to monitor energy storage and consuption from an ic2 power storage block

    Jamsa-2023
--]]

local component = require("component")
local computer = require("computer")
local event = require("event")
local term = require("term")
local sides = require("sides")

local gpu = component.gpu
local beep = component.computer.beep

local bank=component.energy_device

--capacitor
capacity=bank.getCapacity
energy=bank.getEnergy

local w,h =gpu.getResolution()

--timer
local tickCnt = 0
local running = true
local hours = 0
local mins = 0

local function status()
    if capacity() == 0 then 
    return " ded " 
    else
    return " has_joos "
    end
  end 

local function centerF(row, msg, ...)
    local mLen = string.len(msg)
    w, h = gpu.getResolution()
    term.setCursor((w - mLen)/2,row)
    print(msg:format(...))
  end
  --
local function capacitorMax()
    return bank.getCapacity()
    end
    
local function gEnergy()
    if energy() == 0 then
    return "0   "
    else
    return bank.getEnergy()
    end
  end

local function getPercent()
   diff = bank.getEnergy() / capacitorMax() * 100;
   diff = math.floor(diff+0.5)
   return diff;
end

gpu.setForeground(0xffffff)

-----
term.clear()
term.setCursor(1,1)

centerF(5,  "-----------------------------------------")
centerF(6,  "-       power monitor thing        -")
centerF(7,  "-----------------------------------------")
centerF(8, string.format("- PowerBank is:           %s     -",status()))
centerF(9, string.format("- PowerBank Stored:      %s       -",getPercent()))
centerF(10, string.format("- PowerBank maxPower:      %s       -",capacitorMax()))
centerF(11, string.format("- PowerBank stored:        %s        -",  gEnergy()))
centerF(12, "-----------------------------------------")
centerF(13, "-                                       -")   
centerF(14, "-----------------------------------------")

while true do 

  
    tickCnt = tickCnt + 1
    if tickCnt == 60 then
      mins = mins + 1
      tickCnt = 0
    end
    
    if mins == 60 then
      hours = hours + 1
      mins = 0
    end
  
    os.sleep(1)
    
    centerF(8, string.format("- PowerBank is:         %s     -",status()))
    centerF(9, string.format("- PowerBank Stored:      %s       -",getPercent()))
    centerF(10, string.format("- PowerBank maxPower:      %s      -",capacitorMax()))
    centerF(11, string.format("- PowerBank stored:        %s       -",  gEnergy()))
  end
