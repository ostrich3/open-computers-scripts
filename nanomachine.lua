local component = require("component")
local computer = require("computer")
local event = require("event")
local term = require("term")
local sides = require("sides")

local gpu = component.gpu
local beep = component.computer.beep


local modem = component.modem
modem.open(1)
modem.broadcast(1, "nanomachines", "setResponsePort", 1)
